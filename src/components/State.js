import React,{ useState } from 'react';

function State() {
    
    const [count, setcount] = useState(0);

    function decrement() {
        setcount(count - 1)
    }

    function increment() {
        setcount(count + 1)
    }


    return(
        <div>
            <button onClick={decrement} >-</button>
            <span>{count}</span>
            <button onClick={increment}>+</button>
        </div>
    );
}

export default State;