import React, { useState, useEffect } from "react";
import axios from "axios";
import SmartTable from "./SmartTable";

const TextBar = (props) => {
  const [sad, setSad] = useState("");
  const [UserData, setUserData] = useState([]);
  const [error, setError] = useState("");

  //   below USE EFFECT hooks is rendered which means it will
  //   run first when this component mounts because it has an
  //   empty dependency array

  useEffect(() => {
    //inside here we are going to call an mock api for getting list of users

    //using axios package this axios will return promise object

    // A promise is an object which can be returned synchronously from an asynchronous function. It will be in one of 3 possible states:
    // Fulfilled: onFulfilled() will be called (e.g., resolve() was called)
    // Rejected: onRejected() will be called (e.g., reject() was called)

    // below axios get method is called it will return an promise
    // the result will be either success or failed
    // if success it will get into .then(()) or .catch(())

    // to update every 5 seconds

    setInterval(() => {
      axios({
        method: "get",
        url: "https://5f5ca5325e3a4d0016249893.mockapi.io/users",
        responseType: "json",
      })
        .then((response) => {
          console.log(response, "ress");
          setUserData(response.data);
        })
        .catch((error) => {
          setError(error.message);
        });
    }, 5000);

    // axios({
    //   method: "get",
    //   url: "https://5f5ca5325e3a4d0016249893.mockapi.io/users",
    //   responseType: "json",
    // })
    //   .then((response) => {
    //     console.log(response, "ress");
    //     setUserData(response.data);
    //   })
    //   .catch((error) => {
    //     setError(error.message);
    //   });
  }, []);

  return (
    <div style={{ flex: 1 }}>
      <SmartTable userList={UserData} />
    </div>
  );
};

export default TextBar;
