import React, { useState, useEffect } from "react";
import { Menu, Button } from "antd";
import {
  AppstoreOutlined,
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  PieChartOutlined,
  DesktopOutlined,
  ContainerOutlined,
  MailOutlined,
} from "@ant-design/icons";
import TextBar from './TextBar'
const { SubMenu } = Menu;

const Menubar = () => {
  const [collapsed, setCollapsed] = useState(false);
  const[selectedTitle,setSelectedTitle] = useState("Option 1") //option 

  const toggleCollapsed = () => {
    setCollapsed(!collapsed);
  };
  const selectMenu = ({ item, key, keyPath, selectedKeys, domEvent }) => {
    setSelectedTitle(domEvent.target.outerText,"texttttttt")
  }
  return (
    <div>
      <Menu theme="dark" style={{ display: "flex" }}>
        <Menu.Item>
          <Button
            type="primary"
            onClick={toggleCollapsed}
            style={{ marginBottom: 16 }}
          >
            {React.createElement(
              collapsed ? MenuUnfoldOutlined : MenuFoldOutlined
            )}
          </Button>
        </Menu.Item>
        <Menu.Item style={{marginLeft: "auto" }}>
        <Button
            type="primary"
            onClick={toggleCollapsed}
            style={{ marginBottom: 16 }}
          >
            Sign out
          </Button>
        </Menu.Item>
      </Menu>
      <div style={{display:"flex",flexDirection:"row",width:"100%" }}>
        
      <div>
        <Menu
          defaultSelectedKeys={["1"]}
          defaultOpenKeys={["sub1"]}
          mode="inline"
          theme="dark"
          style={{
            display: "flex",
            flexDirection: "column",
            height:window.innerHeight - 53,
            width:collapsed === false ? 200 :100
          }}
          inlineCollapsed={collapsed}
          onClick={selectMenu}
        >
          <Menu.Item key="1" icon={<PieChartOutlined />}>
            Option 1
          </Menu.Item>
          <Menu.Item key="2" icon={<DesktopOutlined />}>
            Option 2
          </Menu.Item>
          <Menu.Item key="3" icon={<ContainerOutlined />}>
            Option 3
          </Menu.Item>
        </Menu>
      </div>
      <TextBar title={selectedTitle}/>
      </div>

    </div>
  );
};

export default Menubar;
