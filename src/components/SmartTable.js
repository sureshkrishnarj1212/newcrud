import React, { useState, useEffect, useMemo } from "react";
import { Drawer, Form, Button, Input } from "antd";
import axios from "axios";

import "./table.css";
const SmartTable = (props) => {
  const [visible, setVisibe] = useState(false);
  const [editData, setEditData] = useState({});

  const data = props.userList;
  const columns = useMemo(() => {
    let headers = [];
    let userHeaders = props.userList[0];
    for (let key in userHeaders) {
      headers.push({
        Header: key,
        accessor: key,
      });
    }
    return headers;
  }, [props.userList]);

  const editItem = (data) => {
    axios({
      method: "get",
      url: `https://5f5ca5325e3a4d0016249893.mockapi.io/users/${data.id}`,
      responseType: "json",
    })
      .then((response) => {
        setEditData(response.data);
        setVisibe(true);
      })
      .catch((error) => {
        // setError(error.message);
      });
  };

  const onClose = () => {
    setEditData({});
    setVisibe(false);
  };
  const onSubmit = (id, values) => {
    setEditData({});
    setVisibe(false);

    console.log(id, values, "submit value");
    axios({
      method: "PUT",
      url: `https://5f5ca5325e3a4d0016249893.mockapi.io/users/${id}`,
      data: {
        id: id,
        Name: values.name,
        Age: values.age,
        Country: values.country,
        State: values.state,
        City: values.city,
      },
    })
      .then((res) => {
        console.log(res, "success");
      })
      .catch((fail) => {
        console.log(fail, "fail");
      });
  };

  //   [
  // {Header: "id", accessor: "id"}
  // {Header: "Name", accessor: "Name"}
  // {Header: "City", accessor: "City"}
  // {Header: "Age", accessor: "Age"}
  // {Header: "State", accessor: "State"}
  // {Header: "Country", accessor: "Country"}

  // ]

  const deleteItem = (item) => {
    console.log(item.id, "deletet");
    axios({
      method: "DELETE",
      url: `https://5f5ca5325e3a4d0016249893.mockapi.io/users/${item.id}`,
    })
      .then((response) => {})
      .catch((error) => {
        // setError(error.message);
      });
  };

  return (
    <div style={{ display: "flex", justifyContent: "center", marginTop: 20 }}>
      <table>
        <tr>
          {columns.map((data) => {
            return <th style={{ textAlign: "center" }}>{data.Header}</th>;
          })}
          <th style={{ textAlign: "center" }}>Edit</th>
          <th style={{ textAlign: "center" }}>Delete</th>
        </tr>

        {/* data = [
            {a:"maris",age:10},

            {a:"prem",age:30}
          ]


          {id: 2 ,a:"maris",age:10} ===> item

          Object.values(item) = [2,"maris",10]
          Object.keys(item) = [id,a,age]


          [2,"maris",10].map((hey) => {}) */}

        {/* 
[
0: {id: "1", Name: "Melinda Sauer", City: "City 1", Age: 53, State: "State 1", Country: "Country 1"}
1: {id: "2", Name: "Noah Marks", City: "City 2", Age: 51, State: "State 2", Country: "NewZealand"}
2: {id: "3", Name: "Stephen Schaden V", City: "City 3", Age: 33, State: "State 3", Country: "Country 3"}
3: {id: "4", Name: "David Muller", City: "City 4", Age: 69, State: "State 4", Country: "Davidbhai"}
4: {id: "5", Name: "Lorenzo McKenzie", City: "City 5", Age: 22, State: "State 5", Country: "Country 5"}
5: {id: "6", Name: "Matthew Gerhold", City: "City 6", Age: 72, State: "State 6", Country: "Country 6"}
6: {id: "7", Name: "Enrique Blanda", City: "City 7", Age: 5, State: "State 7", Country: "Country 7"}
7: {id: "8", Name: "Gordon Williamson", City: "City 8", Age: 78, State: "State 8", Country: "Country 8"}
8: {id: "9", Name: "Traci Murazik", City: "City 9", Age: 67, State: "State 9", Country: "Country 9"}
9: {id: "10", Name: "Joyce Kilback", City: "City 10", Age: 18, State: "State 10", Country: "Country 10"}
10: {id: "11", Name: "Conrad Abbott MD", City: "City 11", Age: 94, State: "State 11",…}] */}

        {data.map((item) => (
          <tr key={item.id}>
            {Object.values(item).map((val) => (
              <td>{val}</td>
            ))}
            <td style={{ textAlign: "center" }}>
              <button
                onClick={() => {
                  editItem(item);
                }}
              >
                Edit
              </button>
            </td>
            <td style={{ textAlign: "center" }}>
              <button
                style={{ backgroundColor: "red", color: "#FFFFFF" }}
                onClick={() => {
                  deleteItem(item);
                }}
              >
                Delete
              </button>
            </td>
          </tr>
        ))}
      </table>
      <Drawer
        title="Edit user details"
        width={400}
        onClose={onClose}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
      >
        {Object.keys(editData).length !== 0 && (
          <Form
            layout="vertical"
            hideRequiredMark
            onFinish={(values) => {
              onSubmit(editData.id, values);
            }}
          >
            <Form.Item
              name="name"
              label="Name"
              rules={[
                {
                  required: editData.Name ? false : true,
                  message: "Please enter user name",
                },
              ]}
              initialValue={editData.Name}
            >
              <Input
                placeholder="Please enter user name"
                defaultValue={editData.Name}
              />
            </Form.Item>
            <Form.Item
              name="city"
              label="City"
              rules={[
                {
                  required: editData.City ? false : true,
                  message: "Please enter city",
                },
              ]}
              initialValue={editData.City}
            >
              <Input
                placeholder="Please enter user name"
                defaultValue={editData.City}
              />
            </Form.Item>
            <Form.Item
              name="age"
              label="Age"
              rules={[
                {
                  required: editData.Age ? false : true,
                  message: "Please enter age",
                },
              ]}
              initialValue={editData.Age}
            >
              <Input
                placeholder="Please enter user name"
                defaultValue={editData.Age}
              />
            </Form.Item>
            <Form.Item
              name="state"
              label="State"
              rules={[
                {
                  required: editData.State ? false : true,
                  message: "Please enter state",
                },
              ]}
              initialValue={editData.State}
            >
              <Input
                placeholder="Please enter user name"
                defaultValue={editData.State}
              />
            </Form.Item>
            <Form.Item
              name="country"
              label="Country"
              rules={[
                {
                  required: editData.Country ? false : true,
                  message: "Please enter country",
                },
              ]}
              initialValue={editData.Country}
            >
              <Input
                placeholder="Please enter user name"
                defaultValue={editData.Country}
              />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        )}
      </Drawer>
    </div>
  );
};

export default SmartTable;
