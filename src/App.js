// import Menubar from './components/Menubar'
import Mobile from './components/Mobile'
import './App.css'
import State from './components/State';
function App() {
  var i = 1;
  var style = {
    color: 'red',
    fontsize: 100
  }
  let n = 1;
  // this.state = {
  //   header : "Hi",
  //   subject : "Hello!"
  // }
  return (
    <div>
      <h1 style={style}>{i == 1 ? 'True!' : 'False'}</h1>
      {/*This is style..*/}
      {/* <h2>{this.state.header}</h2>
      <h2>{this.state.subject}</h2> */}
      <h1>{n}</h1>
      {<Mobile />}
      <State />
    </div>
  );
}

export default App;
